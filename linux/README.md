The flash-v2.sh has not been tested on every single OSes like flash.sh so, if there is/are any problem/s please report to me, thanks!

WHAT'S NEW IN V2:

- Verify if you are running in root or not. so run sudo ./script-v2.sh
- Added a pause function, in order to make you able to read everything carefully before accept
- Use if/elif/else instead of CASE, this makes the script easier to understand for me and for you
