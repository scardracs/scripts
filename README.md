# SCRIPTS for LUX, STYLE and PURE

Hi everyone.
- These scripts are developed and tested by me and released under [GPL-2 license](https://opensource.org/licenses/gpl-2.0.php).
- They are tested for flashing stock ROMs on devices posted above.
- They are NOT tested for other devices, but they *could* work too.
- You can use and/or modify without any limits.
- Any updates or suggestions will be appreciated!
